package io.hexa1;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.*;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.nio.charset.Charset;

public class App {
  public static void main(String [] args) {
    if (args.length != 1) {
      System.out.println("Pass a filepath as command line argument");
      System.exit(0);
    }

    try {
      String       html     = IOUtils.toString(new FileInputStream(args[0]), Charset.forName("UTF-8"));
      OutputStream file     = new FileOutputStream(new File("out.pdf"));
      Document     document = new Document();
      PdfWriter    writer   = PdfWriter.getInstance(document, file);
      MyFooter event = new MyFooter();
      writer.setPageEvent(event);
      document.open();
      InputStream is = new ByteArrayInputStream(html.getBytes());
      XMLWorkerHelper.getInstance().parseXHtml(writer, document, is);
      document.close();
      file.close();
      System.out.println("out.pdf saved");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}

class MyFooter extends PdfPageEventHelper {
  Integer pageNumber = 1;

  public void onEndPage(PdfWriter writer, Document document) {
    PdfContentByte cb     = writer.getDirectContent();
    Phrase         header = new Phrase("this is a header, page: " + pageNumber);
    Phrase         footer = new Phrase("this is a footer, page: "  + pageNumber++);
    ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
      header,
      (document.right() - document.left()) / 2 + document.leftMargin(),
      document.top() + 10, 0);
    ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
      footer,
      (document.right() - document.left()) / 2 + document.leftMargin(),
      document.bottom() - 10, 0);
  }
}
